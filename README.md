# 2023_assignment2_portfolio

Matteo Fiorina 865956\
Tommaso Ferrario 869005\
Simone Vendramini 866229

Repository: <https://gitlab.com/m.fiorina1/2023_assignment2_portfolio>

# Progetto

### System to be

Il progetto in questione consiste nello sviluppo di un'applicazione mobile per
dispositivi Android finalizzata alla gestione delle finanze personali.
L'obiettivo primario che si prefigge tale applicazione è quello di fornire
all'utente uno strumento intuitivo e di facile utilizzo per il monitoraggio e
la gestione delle proprie spese quotidiane.

L'utente, mediante un'interfaccia accessibile, potrà facilmente registrare e
tenere traccia di nuove transazioni finanziarie, consentendo così una visione
esaustiva della propria situazione economica.
Attraverso questa piattaforma, sarà in grado di visualizzare un'analisi
dettagliata dei suoi pattern di spesa, contribuendo in tal modo a una maggiore
consapevolezza e controllo delle proprie finanze personali.

In aggiunta, l'applicazione mira a fornire una soluzione per la gestione delle
spese sostenute tra amici.

### System as is

Il sistema corrente impiegato per la gestione delle finanze personali è
costituito da un foglio di calcolo, il quale consente di monitorare le entrate e
le uscite finanziarie. Tuttavia, tale sistema presenta diversi svantaggi in
termini di usabilità. In particolare, la mancanza di una struttura predefinita
richiede che l'utente la crei autonomamente, complicando l'accesso per coloro
che non possiedono una competenza avanzata nell'utilizzo di strumenti informatici.

Inoltre, l'assenza di un'interfaccia standardizzata comporta la possibilità di
non ottenere una visione completa delle proprie finanze. Infine, va sottolineato
che il sistema attuale non consente una gestione efficiente delle spese condivise
con altre persone.

# Stakeholders

Compreso il dominio di interesse, si è proceduto con l'identificazione degli
stakeholders individuando i seguenti soggetti:

- **Cliente**: il committente che ha affidato l'incarico per lo sviluppo
  dell'applicazione costituisce uno degli attori chiave del progetto. In qualità
  di principale stakeholder, detiene un'influenza significativa sulle decisioni
  e sulle direzioni strategiche adottate durante il processo di sviluppo.
  La sua soddisfazione e approvazione rappresentano elementi fondamentali per
  il successo del progetto. Il coinvolgimento attivo del cliente risulta
  cruciale al fine di garantire un dialogo costante e una comprensione profonda
  delle sue esigenze, aspettative e obiettivi specifici.
- **Utenti Finali**: sono individui che cercano strumenti efficaci per gestire
  le proprie finanze personali. Comprendere le loro esigenze e preferenze è
  cruciale per progettare un'interfaccia facile da usare e garantire la praticità
  e la pertinenza dell'applicazione.
- **Autorità di Regolamentazione**: il rispetto delle normative finanziarie
  pertinenti e delle leggi sulla protezione dei dati è un aspetto fondamentale
  per garantire la sicurezza e la privacy degli utenti.
- **Sviluppatori**: il gruppo responsabile della creazione e del mantenimento
  dell'applicazione gioca un ruolo fondamentale nella realizzazione del progetto.
  La loro competenza e impegno sono essenziali per fornire un'applicazione facile
  da usare, sicura e funzionale che soddisfi le aspettative del cliente e degli
  utenti.
- **Applicazioni concorrenti**: sono applicazioni che offrono funzionalità
  simili a quelle che si vogliono implementare. L'analisi di queste applicazioni
  permette di individuare le funzionalità che sono da considerarsi essenziali
  per l'applicazione che si vuole sviluppare.

Per comprendere meglio l'importanza dei vari stakeholder, si è deciso di
realizzare una classificazione di essi in base al loro potere e interesse nei
confronti del progetto.

<div style="page-break-after: always;"></div>

| Stakeholder                  | Potere | Interesse | Strategia                                                                                                                                                             |
| ---------------------------- | ------ | --------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Cliente                      | Alto   | Alto      | **Fully engage**: _coinvolgimento regolare_                                                                                                                           |
| Utenti Finali                | Basso  | Alto      | **Keep satisfied**: _consultarli spesso cercando di risolvere le problematiche indicate, coinvolgendoli regolarmente per ottenere dettagli e informazioni specifiche_ |
| Autorità di Regolamentazione | Alto   | Basso     | **Keep satisfied**: _il mantenerli informati e soddisfatti ma senza troppi dettagli_                                                                                  |
| Sviluppatori                 | Basso  | Alto      | **Keep satisfied**: _consultarli spesso cercando di risolvere le problematiche indicate, coinvolgendoli regolarmente per ottenere dettagli e informazioni specifiche_ |
| Applicazioni concorrenti     | Basso  | Basso     | **Minimum effort**: _monitorare le loro azioni e le loro strategie_                                                                                                   |

# Workflow

Di seguito è riportata l'analisi che è stata concepita per scoprire i requisiti
dell'applicazione che si vuole realizzare.

Con l'obiettivo di identificare e definire tali requisiti si è optato per
l'impiego di tecniche di elicitazione sia artefact-driven che stakeholders-driven.

Tra le tecniche artefact-driven che sono state considerate troviamo:

- Background Study
- Questionnaires
- Prototyping

Mentre, per quanto riguarda le tecniche stakeholders-driven, si è pianificato
di realizzare un'intervista con il Cliente.

Le tecniche artefact-driven sono state scelte per ottenere una visione generale
e di alto livello dei requisiti, mentre le tecniche stakeholders-driven sono
state scelte per ottenere una visione più dettagliata e specifica dei requisiti.

Al fine di acquisire una comprensione completa e approfondita per l'implementazione
dell'applicazione, si è deciso di avviare un background study.
Questo studio è finalizzato a esplorare, mediante un'analisi dettagliata, il
funzionamento di applicazioni che trattano questioni analoghe. Attraverso questo
processo, si vogliono identificare le principali funzionalità che applicazioni
già esistenti offrono, in modo da presentarsi alla successiva fase di intervista
con il cliente con una conoscenza approfondita del dominio di interesse.

Successivamente a questa fase preliminare, si procederà a contattare il cliente
per organizzare un incontro nel quale sarà svolta un'intervista, volta
a comprendere in modo completo le aspettative e le esigenze specifiche del
cliente relative all'implementazione dell'applicazione. Durante l'intervista,
verranno valutate le informazioni precedentemente acquisite attraverso il
background study con lo scopo di mettere in evidenza le priorità del cliente e
eliminare eventuali funzionalità superflue che sono state identificate.

Sulla base delle informazioni acquisite durante l'intervista, si procederà alla
redazione di un questionario da somministrare agli utenti finali
dell'applicazione. L'obiettivo di questo questionario è raccogliere informazioni
dettagliate sulle loro preferenze e aspettative.

Una volta ottenuti i risultati delle fasi precedenti, si procederà con la
creazione di un prototipo dell'applicazione. Tale prototipo sarà sottoposto al
cliente per ricevere feedback riguardo alla sua struttura e alle sue funzionalità.
In caso di riscontro di un feedback negativo, si provvederà a modificare il
prototipo per risolvere le problematiche individuate.

Nel caso in cui si registri un feedback positivo da parte del cliente, si darà
seguito all'effettiva implementazione della funzionalità, partendo dal prototipo
precedentemente sviluppato e approvato.

Per semplificare la comprensione del workflow, è stata realizzata la seguente
rappresentazione grafica:

```plantuml
start
:Background Study;
:Intervista con il Cliente;
:Questionario per gli utenti;
repeat
:Creazione di un prototipo;
repeat while (Feedback positivo?) is (no)
-> sì;
stop
```

## Background Study

### Requisiti

Durante la fase di background study si vogliono identificare sia requisiti
funzionali che requisiti non funzionali.

Tra i requisiti funzionali, vogliamo identificare le principali funzionalità
che l'applicazione deve offrire e in quale modo sono simili o si differenziano
da applicazioni già esistenti. In particolare, vogliamo identificare i requisiti
funzionali relativi a:

- Gestione delle transazioni
- Interazione con altri utenti
- Gestione delle integrazioni con altre applicazioni

Mentre, per quanto riguarda i requisiti non funzionali vogliamo ricavare le
informazioni relative a:

- Sicurezza
- Privacy
- Performance

In conclusione, si intende identificare le possibili normative che disciplinano
il trattamento dei dati degli utenti dell'applicazione, sia a livello personale
che finanziario.

### Descrizione

L'obiettivo di questo studio è quello di raccogliere informazioni dettagliate
sul dominio di interesse, in modo da avere una visione completa delle
funzionalità che l'applicazione deve offrire e delle normative che disciplinano
il trattamento dei dati degli utenti.

Questa fase si concentra sull'analisi di applicazioni esistenti che
trattano lo stesso dominio di interesse, in modo da identificare le funzionalità
che sono da considerarsi essenziali per l'applicazione che si vuole sviluppare.

Le risorse che possono essere utilizzate per l'analisi sono relative alla
documentazione delle varie applicazioni, disponibile sulle rispettive pagine web,
come ad esempio [Splitwise](https://www.splitwise.com/) per la gestione delle
spese condivise e [Money Manager](https://realbyteapps.com/) per la gestione
delle finanze personali.

Inoltre, saranno esaminati gli aspetti tecnici relativi all'interfaccia utente
all'esperienza di utilizzo offerta dalle varie applicazioni.

Per quanto riguarda i requisiti relativi alla parte legale, possono essere
ottenuti consultando le normative vigenti in materia di protezione dei dati e
privacy, ad esempio a livello europeo consultando il [GDPR](https://gdpr.eu/).

### Stakeholder

In questa attività di scoperta dei requisiti sono coinvolti l'autorità di
regolamentazione e le applicazioni concorrenti.

### Ragione

L'analisi approfondita dei requisiti funzionali e non funzionali, insieme ai
vincoli normativi, fornirà una base solida per presentarsi nella successiva fase
di intervista con una approfondita conoscenza del dominio di interesse.
Inoltre, questa fase permette di definire una base di conoscenze comune tra gli
sviluppatori, facilitando la comunicazione e la comprensione reciproca e
fornendo una base solida per il futuro sviluppo del progetto.

## Intervista con il Cliente

### Requisiti

Con questa tecnica di elicitazione si vuole avere un feedback sui requisiti funzionali scoperti attraverso il background study e eventualmente identificarne di nuovi.
Si vogliono individuare requisiti funzionali relativi a:

- Gestione delle transazioni
- Interazione con altri utenti
- Gestione delle integrazioni con altre applicazioni

### Descrizione

Al termine del background study, si procederà con l'intervista al cliente. Lo scopo di questa intervista è quello di ottenere un feedback sulle informazioni raccolte durante il background study e di identificare eventuali requisiti che non sono stati individuati durante la fase precedente.

Per i vari requisiti identificati durante il processo dell'intervista si procederà alla loro classificazione in base alla priorità e alla fattibilità. Inoltre, si cercherà di identificare eventuali dipendenze tra i vari requisiti.

Con questa fase si vuole coinvolgere il cliente per comprendere le sue esigenze
e le sue aspettative sul progetto.

Tra gli argomenti che si vogliono affrontare troviamo:

- Le funzionalità chiave per la gestione delle transazioni.
- Aspetti riguardanti l'interfaccia utente, inclusa l'usabilità e
  l'esperienza complessiva.
- Possibili requisiti tecnici e vincoli di implementazione.

Infine, per ogni requisito identificato, si cercherà di ottenere informazioni
utili per l'identificazione di requisiti non funzionali.

### Stakeholder

In questa attività di elicitazione dei requisiti è coinvolto il cliente.

### Ragione

L'intervista è un passo cruciale del workflow, in quanto permette di delineare
i requisiti specifici del cliente. Comprendere appieno le sue esigenze è un
passo essenziale per comprendere come strutturare lo sviluppo del progetto,
in modo da ridurre al minimo il rischio di errori nello sviluppo del progetto.

## Questionario per gli utenti dell'applicazione

### Requisiti

Si vogliono identificare requisiti legati all'esperienza che l'utente si aspetta
di ottenere dall'applicazione. Ci aspettiamo di aggiungere un livello di
dettaglio maggiore rispetto ai requisiti scoperti nelle fasi precedenti,
con particolare attenzione sul come deve essere implementata l'interfaccia
grafica per garantire la migliore esperienza possibile.
Inoltre vogliamo verificare l'opinione degli utenti a riguardo di alcune decisioni del cliente.

Ci aspettiamo di ottenere requisiti funzionali relativi all'interazione con
l'applicazione (come deve essere strutturata l'interfaccia grafica).
Inoltre vogliamo individuare a quali categorie gli utenti finali possano appartenere, per poter promuovere al meglio l'applicazione.

### Descrizione

Lo scopo di questa attività è quello di ottenere un feedback dagli utenti in
modo da renderli partecipi del processo di sviluppo dell'applicazione.
Questo passo permette di creare un prodotto che soddisfi da subito le
aspettative degli utenti che intendono utilizzare l'applicazione.

Tramite le domande presenti nel questionario si vuole ottenere un feedback sulle funzionalità e
sulla struttura dell'interfaccia grafica dell'applicazione per poter
perfezionare i requisiti già individuati.

### Stakeholder

Gli stakeholder coinvolti in questa attività sono gli utenti finali che
pensano di utilizzare l'applicazione.

### Ragione

L'utilizzo di un questionario consente di ottenere un feedback da parte
degli utenti permettendo agli sviluppatori di comprendere meglio le loro
aspettative e quindi creare un prodotto che sia allineato alle loro aspettative ed esigenze.

## Creazione di un prototipo

### Requisiti

Con questa attività ci aspettiamo di scoprire requisiti legati allo sviluppo
dell'applicazione, come ad esempio la scelta delle tecnologie da utilizzare.
Inoltre, si vogliono identificare eventuali requisiti non funzionali che non
sono stati individuati durante l'intervista con il cliente e il background study.

### Descrizione

In questa fase, ci si aspetta di tradurre i requisiti principali identificati
durante l'intervista con il cliente in una implementazione temporanea, al fine
di ottenere un feedback per verificare la corretta comprensione dei requisiti
raccolti nell'intervista.

Il prototipo dovrebbe riflettere le funzionalità di base e l'esperienza utente
desiderata, offrendo una rappresentazione visiva e pratica dell'applicazione.

Data la volontà di riutilizzare il prototipo nella successiva fase di
implementazione, è necessario che venga sviluppato in modo tale da essere di
semplice comprensione.

### Stakeholder

Gli stakeholder coinvolti per la realizzazione di questa attività sono il
cliente e gli sviluppatori.

### Ragione

L'idea alla base della realizzazione di un prototipo è quella di ottenere un
feedback da parte del cliente sulla comprensione dei requisiti raccolti durante
l'intervista. Il prototipo che verrà realizzato consentirà al cliente di
visualizzare e valutare l'aspetto visivo e le funzionalità di base,
facilitando la comprensione e la comunicazione tra le parti coinvolte.

Inoltre, il prototipo può essere utilizzato come base per lo sviluppo
dell'applicazione finale, riducendo così il tempo e lo sforzo necessario per la
sua realizzazione.

# Implementazione dettagliata

Si è deciso di implementare dettagliatamente l'attività concernente l'intervista
al Cliente. La scelta del soggetto da intervistare è stata agevolata dalla presenza di un unico cliente.

La decisione di condurre un'intervista con il cliente è stata presa con
l'obiettivo di avviare lo sviluppo dell'applicazione e perseguire
un'implementazione ottimale della sua concezione del progetto.

### Struttura dell'intervista

Per ottenere i requisiti abbiamo deciso di intervistare il Cliente che ha
commissionato l'applicazione in modo da modellare questa sezione secondo
le sue preferenze.

Per questa intervista sono state preparate alcune domande per individuare i
requisiti principali, mentre altre sono state formulate in seguito alle risposte
ottenute:

#### Domande a carattere generico

Le domande a carattere generico sono interrogativi che esplorano il progetto da
una prospettiva più ampia

1. Qual è l'obbiettivo principale dell'applicazione?\
   Tramite questa domanda si vuole capire quale deve essere il focus, nonché
   punto di forza del progetto.
1. Come deve essere strutturata l'interfaccia principale?\
   Questa domanda torna utile per dedurre la modalità ideale per organizzare o
   progettare l'interfaccia principale in base alle preferenze dello stakeholder.

#### Domande relative alla gestione delle transazioni

Le domande relative alla gestione delle transazioni servono a capire come gestire
la parte sull'inserimento, mantenimento e visualizzazione delle transazioni.

1. Cosa devono contenere le transazioni?\
   Vogliamo scoprire quali sono i campi essenziali che il cliente ritiene
   necessari a rappresentare le transazioni.
1. Come devono essere salvate e visualizzate le varie transazioni?\
   Vogliamo capire come il cliente voglia che le transazioni siano memorizzate
   e mostrate agli utenti finali
1. Come ci si comporta in caso di errore nell'inserimento di una transazione?\
   Con questa domanda apparentemente banale vogliamo capire se le transazioni
   devono poter essere modificate o solamente cancellate, essendo che potrebbe
   rendere la struttura complicata durante le interazioni tra più utenti.
1. Come specifico degli eventuali commenti per le varie transazioni?\
   Questa domanda è sorta durante l'intervista essendo che ci è sembrato utile
   poter annotare qualcosa in modo opzionale per le transazioni, dato che il
   cliente non ha specificato niente a riguardo.

#### Domande sorte durante l'intervista sulla gestione dei grafici e delle categorie

Le domande sui grafici non erano previste durante la prima intervista con il
cliente, ma sono sorte spontanee dopo che lo stakeholder ha espresso la
necessità della loro presenza all'interno dell'applicazione.

1. Come devono essere gestite le categorie?\
   È utile sapere che idea si sia fatto il cliente su queste categorie, quindi
   vorremmo capire come gestirle.
1. Come devono essere presentati i grafici?\
   Domanda autoesplicativa, serve a capire quali grafici vuole il cliente.

#### Domande relative alle interazioni con altri utenti dell'app

Una meccanica presente molto spesso nelle app che abbiamo utilizzato per la
fase di background study comportano delle interazioni con altri utenti dell'app,
quindi abbiamo deciso di indagare anche su questa possibile necessità.

1. Deve essere gestita una interazione con gli amici?\
   Domanda preliminare per capire se il cliente ha intenzione di gestire anche
   le interazioni tra contatti.
1. Come deve essere gestita l'integrazione tra amici?\
   Questa domanda serve a capire come andrebbero gestiti gli scambi di denaro
   tra amici secondo lo stakeholder.
1. È previsto l'utilizzo di gruppi?\
   Domanda preliminare per capire se il cliente ha intenzione di gestire anche
   le interazioni tra gruppi.
1. Quali operazioni andrebbero gestite all'interno di un gruppo?\
   Questa domanda serve a capire quali operazioni il cliente vorrebbe gestire
   all'interno di un gruppo. Essendo che alla domanda precedente ci è stato
   risposto in maniera negativa, questa non è stata posta.
1. Come vuoi che venga mostrata la possibilità di aggiungere un creditore?\
   Lo scopo di questa domanda è quella di capire come dovremmo presentare agli
   utenti la possibilità di aggiungere un contatto a cui associare la
   transazione, dal momento in cui nelle domande precedenti non è stata gestita
   questa parte.

#### Domande relative alla visualizzazione dei contatti

Questo gruppo di domande sono volte allo sviluppo dell'interfaccia grafica per
mostrare i contatti da selezionare in una transazione.

1. Come vuoi che vengano mostrati i contatti già salvati?\
   Questa domanda serve a capire come i contatti debbano essere visualizzati
   una volta specificato che si vuole aggiungere un riferimento per la transazione.
1. In che ordine devono essere mostrati i contatti?\
   La particolarità di questa domanda è che ci potrebbero essere varie modalità
   per proporre i primi contatti visualizzati, ad esempio i contatti con
   transazioni più frequenti oppure i contatti preferiti.
1. Che informazioni devono essere mostrate per ogni contatto?\
   Questa domanda è stata posta in quanto l'applicazione vuole essere il più
   semplice possibile, quindi volevamo capire per il cliente quanto si
   dovesse essere sintetici.

#### Integrazioni verso applicazioni esterne

1. Deve essere gestita la possibilità di esportare dei dati?\
   In molte app analizzate durante il background study è presente la possibilità
   di esportare i dati in diversi formati, quindi volevamo sapere se anche per
   questa applicazione fosse prevista.

### Requisiti

Attraverso l'impiego di questa metodologia di elicitazione dei requisiti, ci
prefiggiamo di acquisire dettagliate informazioni inerenti ai requisiti
funzionali e non funzionali dell'applicazione in esame. Nello specifico, ci
attendiamo di ottenere dettagliate informazioni relative a:

- La configurazione dell'interfaccia principale dell'applicazione
- Le modalità di gestione delle transazioni
- La presentazione visiva del bilancio delle transazioni
- Le dinamiche interattive con altri utenti
- Le integrazioni con applicazioni esterne

Parallelamente, intendiamo acquisire conoscenze approfondite in merito ai
requisiti non funzionali, con particolare riferimento alla performance del sistema.

Inoltre, vogliamo riuscire a fornire delle priorità ai requisiti individuati,
in modo da poterli implementare in ordine di importanza.

### Stakeholder

La realizzazione di questa attività ha coinvolto attivamente il cliente,
identificato come il principale stakeholder del progetto. Al fine di acquisire
una comprensione approfondita delle sue esigenze e aspettative, è stato
selezionato come soggetto per l'intervista.

Data la sua posizione di committente dell'applicazione, il cliente riveste un
ruolo di primaria importanza nella definizione dei requisiti e delle
funzionalità dell'applicazione stessa. Al momento dell'avvio del progetto,
il cliente ha fornito i suoi recapiti, agevolando la possibilità di
contattarlo in caso di necessità. Pertanto, attraverso l'impiego di tali
informazioni, è possibile pianificare e organizzare incontri per condurre
interviste finalizzate al sostegno delle fasi di analisi e sviluppo del progetto.

# Risultati dell'intervista

## Raw Data

In seguito, è presentata la trascrizione di alcuni punti chiave dell'intervista
del cliente.

### Raw Data - Domande introduttive

1. Qual è l'obbiettivo principale dell'applicazione?\
   L'applicazione deve essere pensata per gestire il denaro, annotando le entrate
   e le uscite rapidamente.

1. Come deve essere strutturata l'interfaccia principale?\
   L'interfaccia principale deve essere in grado di gestire tutte le casistiche
   principali per l'aggiunta di una nuova transazione.

### Raw Data - Gestione delle transazioni

1. Cosa devono contenere le transazioni?\
   Le transazioni devono contenere il saldo, la data e un titolo.
   Inoltre, deve essere presente un campo opzionale per inserire un eventuale
   destinatario.

1. Come devono essere salvate e visualizzate le varie transazioni?\
   Deve essere presente uno storico dei movimenti suddivisi per periodo.
   Inoltre, è richiesta la presenza di grafici per rendere intuitivi le tipologie
   di spese, ad esempio un grafico a torta diviso per categoria.

1. Come ci si comporta in caso di errore nell'inserimento di una transazione?\
   Deve essere possibile modificare/eliminare il movimento se è sbagliato.

1. Come specifico degli eventuali commenti per le varie transazioni?\
   Devono essere presenti delle note correlate ai pagamenti.

### Raw Data - Grafici e categorie

1. Come devono essere gestite le categorie?\
   L’app dovrebbe essere in grado di capire la categoria in autonomia basandosi
   sui nomi delle operazioni.

1. Come devono essere presentati i grafici?\
   I grafici devono essere accuratamente integrati all'interno di una sezione
   appositamente dedicata sulla homepage dell'applicazione. Tale sezione dovrà
   includere un grafico a torta, che consentirà agli utenti di esaminare le spese
   in modo dettagliato e categorizzato. In aggiunta, si rende necessaria la
   presenza di un grafico a linee, il quale fornirà una rappresentazione visiva
   dell'andamento temporale delle spese. Un ulteriore elemento consiste in un
   grafico dotato di retta di regressione, volto a evidenziare la tendenza delle
   transazioni in un arco temporale selezionabile dall'utente, ad esempio,
   un mese o un anno.

   All'interno della medesima sezione, è imperativo inserire il bilancio totale.
   Tale indicatore finanziario dovrà essere disposto in modo strategico,
   garantendo una visibilità chiara e immediata senza risultare invasivo.

   Per completare il quadro, si richiede che i grafici siano interattivi,
   consentendo agli utenti di esplorare i dettagli delle transazioni mediante la
   selezione di specifiche porzioni grafiche. È inoltre fondamentale che venga
   presentato un solo grafico alla volta, offrendo agli utenti la facoltà di
   selezionare quale di essi desiderano visualizzare in un determinato momento.

### Raw Data - Interazioni con altri utenti

1. Deve essere gestita una interazione con gli amici?\
   Ci deve essere una possibilità di gestire debiti e accrediti con amici.
   In particolar modo si entra in debito con qualcuno solo dopo una certa
   threshold (se l'importo dovuto supera la threshold viene segnalato).

1. Come deve essere gestita l'integrazione tra amici?\
   Deve essere presente la possibilità di aggiungere persone per gestire debiti/crediti.
   Sarebbe bello poter aggiungere delle sincronizzazioni, potendo inserire
   un prestito tra amici da un dispositivo solo.

1. È previsto l'utilizzo di gruppi?\
   L'applicazione è pensata ad uso personale, quindi i gruppi non dovrebbero
   essere presenti.

1. Come vuoi che venga mostrata la possibilità di aggiungere un creditore?\
   Voglio una sezione che si espande per visualizzare i contatti.
   Se il creditore non è presente nella lista dei contatti deve essere presente
   l'opzione di aggiungere una nuova persona.

### Raw Data - Visualizzazione dei contatti

1. Come vuoi che vengano mostrati i contatti già salvati?\
   Devono apparire tutti i contatti ma con la possibilità di effettuare una
   ricerca.

1. In che ordine devono essere mostrati i contatti?\
   I contatti con più interazioni devono essere mostrati per primi.

1. Che informazioni devono essere mostrate per ogni contatto?\
   Nome e Cognome.

### Raw Data - Integrazioni verso applicazioni esterne

1. Deve essere gestita la possibilità di esportare dei dati?
   Deve essere possibile esportare in Excel.

## Analisi dei dati

Le informazioni raccolte durante l'intervista mirano a enfatizzare l'approccio
intuitivo e accessibile dell'applicazione proposta. Secondo il resoconto del
cliente, l'architettura dell'applicazione è stata delineata per garantire
un'interfaccia utente semplice e di facile utilizzo.

L'accento è stato posto sull'eliminazione di complessità e ostacoli superflui,
al fine di agevolare un'esperienza fluida e intuitiva per gli utenti finali,
favorendo così l’adozione del prodotto e la massima soddisfazione degli utenti.

In questa intervista abbiamo definito le richieste del cliente sulla maggior
parte delle funzionalità dell'applicazione.

L'intervista è stata strutturata in varie sezioni:

1. **Obiettivi dell'applicazione**: in questa fase si è voluto identificare il
   problema che l'applicazione si propone di risolvere.
1. **Gestione delle transazioni**: in questa sezione si è voluto capire come
   l'applicazione deve gestire le transazioni. Oltre alla fase di inserimento
   e modifica delle transazioni.
1. **Grafici e categorie**: in questa sezione vogliamo scoprire i requisiti
   legati ai grafici e alle visualizzazioni per le transazioni.
1. **Interazioni con altri utenti**: in questa sezione abbiamo definito come
   l'applicazione deve gestire le interazioni con altri utenti.
1. **Integrazioni verso applicazioni esterne**: in questa sezione abbiamo
   definito come l'applicazione deve interagire con altre applicazioni.

### Obiettivi dell'applicazione

L'applicazione è progettata per gestire il denaro, registrando rapidamente le
entrate e le uscite. Dovrebbe essere possibile aggiungere una nuova transazione
in pochi passaggi e visualizzare i dati.

L'interfaccia principale dovrebbe essere in grado di gestire tutte le casistiche
principali per l'aggiunta di una nuova transazione. Inoltre, essendo
l'applicazione pensata per la gestione del denaro, è necessario che sia possibile
visualizzare i dati in modo immediato, per consentire all'utente di avere un
quadro completo della propria situazione finanziaria.

### Gestione delle transazioni

Le transazioni vengono inserite manualmente e devono contenere le informazioni
relative al saldo, alla data e a un titolo. Inoltre, per gestire le transazioni
verso un amico è presente un campo opzionale per inserire un eventuale
destinatario. Le transazioni devono essere salvate e visualizzate in uno storico
dei movimenti suddivisi per periodo, con grafici per rendere intuitive le
tipologie di spese. In caso di errore nell'inserimento di una transazione, deve
essere possibile modificarla o eliminarla. Le transazioni possono avere note
correlate ai pagamenti.

### Grafici e categorie

Ad ogni transazione deve essere associata una categoria, la quale deve essere
assegnata in modo automatico basandosi sulle informazioni presenti nel nome
della transazione. Inoltre, l'utente deve avere la possibilità di modificare la
categoria assegnata.

La visualizzazione delle transazioni deve essere presentata all'utente attraverso
l'utilizzo di grafici. Questa visualizzazione deve essere completa e deve
permettere all’utente di avere una visualizzazione esaustiva delle proprie
transazioni. I grafici che sono stati identificati sono:

- **Grafico a torta**: questo grafico deve mostrare la percentuale di spesa per
  ogni categoria.
- **Grafico a linee**: questo grafico deve mostrare l'andamento delle spese nel
  tempo.
- **Retta di regressione**: questo grafico deve mostrare la tendenza delle spese
  dell'utente in un periodo di tempo selezionato dall'utente.

I grafici devono essere interattivi per permettere all'utente di capire a cosa
si riferisce ogni dato. I grafici devono essere visualizzati nella schermata
principale dell’applicazione uno alla volta, permettendo all’utente di scegliere
quale grafico visualizzare. Oltre al grafico, deve essere visualizzato il
periodo a cui si riferisce e il bilancio totale.

### Interazioni con altri utenti

L'applicazione deve permettere di gestire debiti e accrediti con amici,
segnalando un debito solamente quando si supera una soglia stabilita.

La selezione di un contatto avviene mostrando i contatti in una sezione
espandibile, in questa sezione è anche possibile aggiungere un nuovo contatto
nel caso in cui non sia presente nella lista dei contatti.

Nel caso in cui il contatto non sia presente nella lista di amici, deve essere
possibile aggiungere persone per gestire debiti/crediti.

Deve essere inoltre possibile sincronizzare un prestito tra amici da un solo
dispositivo.

Non è prevista la gestione di transazioni per gruppi.

### Visualizzazione dei contatti

Quando si aggiunge una nuova transazione e si vuole aggiungere un destinatario,
i contatti devono essere mostrati in una lista. Attraverso questa
visualizzazione deve essere possibile cercare un contatto.

Nella lista, i contatti devono essere ordinati in base al numero di interazioni,
posizionando per primi i contatti con più interazioni.

Per ognuno devono essere mostrati nome e cognome.

### Integrazioni verso applicazioni esterne

Durante l'intervista non sono emerse informazioni sull'interazione con
applicazioni esterne, come ad esempio la possibilità di leggere le notifiche di
pagamento di un conto corrente. Sono però emerse informazioni riguardo la
possibilità di esportare e importare i dati presenti in fogli di calcolo (ad esempio Excel).

## Requisiti estratti

I requisiti che abbiamo identificato sono i seguenti:

- L'applicazione deve consentire agli utenti di gestire le proprie finanze,
  tenendo traccia delle entrate e delle uscite in modo rapido.
- L'interfaccia principale dell'applicazione deve essere strutturata in modo
  tale da consentire agli utenti di aggiungere nuove transazioni.
- Le transazioni devono contenere informazioni essenziali come data, titolo,
  importo, un campo per le note e opzionalmente un campo per il destinatario.
- Deve essere possibile modificare ed eliminare le transazioni errate.
- Le transazioni devono essere salvate e visualizzate in uno storico,
  navigabile per periodo.
- È richiesta la presenza di grafici, tra cui un grafico a torta per le
  categorie di spese, una visualizzazione del bilancio e un grafico con retta di
  regressione per mostrare l'andamento nel tempo.
- Deve essere possibile gestire debiti e accrediti con amici.
- Debiti e crediti devono essere sincronizzati con i contatti, se utilizzano
  l'applicazione.
- Quando un debito supera una soglia specifica viene generato un avviso.
- I contatti devono essere visualizzati in un elenco con la possibilità di ricerca.
- L'ordine di visualizzazione dipende dal numero di interazioni.
- Devono essere mostrati solo nome e cognome per ogni contatto.
- L'applicazione deve consentire l'esportazione dei dati in un formato
  compatibile con Excel.

## Introduzione Questionario

A seguito dell'intervista con il cliente, si è deciso di realizzare un
questionario per ottenere un parere sulle funzionalità dell'applicazione da parte
degli utenti finali.

Il questionario è stato strutturato in modo da ottenere informazioni dettagliate
sulle preferenze degli utenti riguardo le funzionalità dell'applicazione. Con
questa attività si cercano delle conferme sui requisiti raccolti nelle fasi
precedenti, si è quindi optato per delle domande a risposta multipla.

## Struttura Questionario

Dopo l'intervista con il cliente, è stato elaborato un questionario con
l'obiettivo di valutare il potenziale delle proposte avanzate dal cliente e
identificare una potenziale clientela a cui presentare l'applicazione.

In seguito, riportiamo le varie sezioni proposte durante il sondaggio, con alcune
delle domande più rilevanti.

#### Domande preliminari

Le domande iniziali svolgono un ruolo cruciale nel delineare la categoria di
appartenenza di un potenziale utente e, di conseguenza, guidano le decisioni di
sponsorizzazione dell'app. Attraverso un approfondito interrogatorio, è possibile
ottenere informazioni cruciali per identificare i partner e le opportunità di
sponsorizzazione più adatte al pubblico target.

1. In che fascia di età ti ritrovi?\
   Le opzioni per questa domanda sono minorenni, 18-25 anni, 25-40 anni e 40 anni in su.
1. Qual è la tua attuale situazione occupazionale o di studio?\
   Le opzioni per questa domanda sono "Lavoro a tempo pieno", "Lavoro part time",
   "Studente a tempo pieno", "Studente part time" o altro.
1. Utilizzerei volentieri una app per gestire le mie finanze in maniera rapida ed efficace\
   Per questa richiesta abbiamo utilizzato un indice di gradimento da 1 a 5.
1. Per cosa utilizzeresti una applicazione come JustSaveMoney?\
   Per questa domanda abbiamo inserito le principali motivazioni di utilizzo dell'applicazione.

#### Gestione delle transazioni

In questa sezione, vengono presentate alcune domande derivate dai requisiti emersi durante l'intervista con il cliente. L'obiettivo è valutare l'efficacia di queste proposte di domande nel coinvolgere e comprendere la clientela.

1. La prima interfaccia proposta appena aperta l'applicazione deve essere l'inserimento di una nuova transazione\
   Dato che la proposta del cliente si discosta dagli esempi del background-study,
   abbiamo deciso di raccogliere l'opinione degli utenti per valutare la validità di questa scelta.

1. Le transazioni devono contenere solo le informazioni essenziali, quali data, titolo, importo e opzionalmente un destinatario.\
   La domanda è stata formulata al fine di valutare la soddisfazione degli
   utenti riguardo alle opzioni di personalizzazione delle transazioni. Nel caso
   di insoddisfazione, è disponibile un campo successivo per fornire eventuali suggerimenti o consigli.

#### Grafici

In questa sezione, abbiamo esaminato l'accoglienza degli utenti riguardo alle
proposte del cliente per la rappresentazione grafica delle transazioni. Poiché
i risultati sono stati estremamente positivi, non riportiamo ulteriori dettagli su queste domande.

#### Interazioni con altri utenti

In questa categoria, esaminiamo la proposta di interazione con altri utenti,
con particolare attenzione all'opzione di threshold personalizzabile presentata dal cliente.

1. Deve essere presente la possibilità di gestire debiti e accrediti con amici.\
   In questa domanda, stiamo valutando quanto sia rilevante per gli utenti gestire
   le interazioni con gli amici.
1. Si dovrebbe entrare in credito con un amico solo dopo aver superato una certa threshold personalizzabile.\
   In questa domanda, stiamo valutando quanto sia rilevante per gli utenti
   la proposta della threshold per gestire i debiti/crediti tra utenti.

#### Visualizzazione dei contatti

Come per la sezione sui grafici in questa sezione abbiamo esaminato il contento
degli utenti delle proposte del cliente. Poiché i risultati non mostrano
particolarità interessanti, non riportiamo ulteriori dettagli.

## Risultati Questionario

In questa sezione proponiamo i risultati del questionario.

#### Domande preliminari

Dalle domande preliminari è sorto che la categoria di utenti da cui abbiamo riscontrato più interesse sono gli studenti a tempo pieno tra i 18 ed i 25 anni.Inoltre abbiamo riscontrato parecchio interesse per due motivazioni con cui tornerà utile promuovere l'applicazione.
In seguito sono riportati i grafici che mostrano quali utenti si sono resi disponibili al questionario.

![Grafico età](Grafici/grafico_età.png)\
Nel grafico è riportata la distribuzione dell'età di chi ha compilato il questionario.\
![Grafico lavori](Grafici/grafico_lavori.jpg)\
Nel grafico sono riportate le posizioni occupate degli intervistati.\
![Grafico preferenze](Grafici/grafico_preferenze.png)\
Nel grafico sono mostrate le motivazioni per cui un utente possa utilizzare l'applicazione.

#### Gestione delle transazioni

Per l'analisi sulla parte inerente alla gestione delle transazioni è sorto che per gli utenti potrebbe essere scomodo la gestione della prima pagina dell'applicazione pensata dal cliente. Di conseguenza proporremo una soluzione che cerchi di andare in contro ad entrambe le linee di pensiero.
Per quanto concerne la personalizzazione delle transazioni gli utenti sono soddisfatti delle proposte effettuate.

![Grafico prima pagina](Grafici/grafico_prima_pagina.png)\
Il grafico rappresenta l'indice di gradimento (1 Disaccordo - 5 Totalmente d'accordo) della seguente domanda: "La prima interfaccia proposta appena aperta l'applicazione deve essere l'inserimento di una nuova transazione"

#### Grafici

I risultati ottenuti sulla sezione dei grafici sono in linea con le proposte
effettuate. L' unico appunto è che gli utenti sembrano preferire il grafico a
torta rispetto alla retta di regressione per visualizzare le transazioni.
Nell'applicazione presenteremo quindi prima il grafico a torta e successivamente il bilancio.

#### Interazioni con altri utenti

Per la sezione delle interazioni gli utenti hanno risposto in maniera affermativa
alla proposta di gestire dei contatti, ma l'idea di proporre una threshold per
gestire il debito/credito tra gli utenti ha ottenuto opinioni contrastanti.
La nostra proposta è quindi quella di implementare la threshold come feature
secondaria, impostata a zero di default, risultando quindi inesistente per
gli utenti che non la desiderano.

![Grafico threshold](Grafici/grafico_thereshold.png)\
Il grafico rappresenta l'indice di gradimento (1 Disaccordo - 5 Totalmente
d'accordo) della seguente domanda: "Si dovrebbe entrare in credito con un amico
solo dopo aver superato una certa threshold personalizzabile"

#### Visualizzazione dei contatti

Tutte le proposte per la visualizzazione dei contatti sono state ben accolte.
In particolare, un utente ha sollevato un'interessante problematica: la
possibilità che due contatti abbiano lo stesso nome e cognome. Proporremo al
cliente l'opzione di considerare l'aggiunta del numero di telefono per una
gestione più efficace in questi casi.

## Stakeholder coinvolti

In questa attività sono coinvolti gli utenti finali dell'applicazione. Per
raggiungere un numero di utenti sufficiente, il questionario è stato condiviso
attraverso le piattaforme social dell'applicazione che sono state create dal
cliente. Inoltre, il questionario è stato condiviso attraverso i social dei
membri del gruppo. Questo è stato fatto per raggiungere un numero di utenti
sufficiente per ottenere un feedback significativo.
